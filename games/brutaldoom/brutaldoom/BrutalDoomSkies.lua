BRUTALDOOM.SKIES = 
        {
            any = 
            {
             'RSKY1'
            }
            tech =
            {
             'SKYPHO'
             'SKYIJ1'
             'SKYIJ2'
             'SKYIJ3'
             'SKYIO1'
             'SKYDEI'
            }
            urban =
            {
             'RSKY2'
             'SKYCIT1'
             'SKYCITH'
            }
            hell =
            {
             'RSKY3'
             'SKYHEL'
             'SKYHEL1'
             'SKYHELF'
            }
            satanshankerchief =
            {
             'CHEXSKY1'
             'CHEXSKY2'
             'CHEXSKY3'
            }
        }
