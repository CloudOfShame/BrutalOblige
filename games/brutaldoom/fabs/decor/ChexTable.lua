--
-- Tables with stools
--

PREFABS.Chex_Table1 =
{
  file   = "decor/ChexTable.wad"
  map    = "MAP01"

  prob   = 255
  theme  = "satanshankerchief"
  --env    = "building"

  where  = "point"
  size   = 128

  bound_z1 = 0
  bound_z2 = 128
}

