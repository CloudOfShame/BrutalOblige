EDAY.MATERIALS =
{
    --wolfenstein
    BRICK28  = { t="BRICK28",    f="BRICK28" }
    BRICK26  = { t="BRICK26",    f="BRICK26" }
    BRICK01  = { t="BRICK01",    f="BRICK01" }
    URFLAT09  = { t="URFLAT09",    f="URFLAT09" }
    ZZWOLF4  = { t="ZZWOLF4",    f="ZZWOLF4" }
    ZZWOLF5  = { t="ZZWOLF5",    f="ZZWOLF5" }
    ZZWOLF9  = { t="ZZWOLF9",    f="ZZWOLF9" }
    SNOW1  = { t="SNOW1",    f="SNOW1" }
    SNOW2  = { t="SNOW2",    f="SNOW2" }
    SNOW3  = { t="SNOW3",    f="SNOW3" }
    RROCK13  = { t="RROCK13",    f="RROCK13" }
    B0130  = { t="B0130",    f="B0130" }
}
