EDAY.MONSTERS =
{
    CyberdemonLordMinor =
    {
        id = 8753
        r = 50
        h = 280
        level = 7
        boss_type = DOOM.MONSTERS.Cyberdemon.boss_type
        boss_prob = DOOM.MONSTERS.Cyberdemon.boss_prob
        prob = DOOM.MONSTERS.Cyberdemon.prob
        crazy_prob = DOOM.MONSTERS.Cyberdemon.crazy_prob
        health = 8000
        damage = DOOM.MONSTERS.Cyberdemon.damage
        attack = DOOM.MONSTERS.Cyberdemon.attack
        density = DOOM.MONSTERS.Cyberdemon.density
        weap_min_damage = DOOM.MONSTERS.Cyberdemon.weap_min_damage
        weap_prefs = DOOM.MONSTERS.Cyberdemon.weap_prefs
        room_size = DOOM.MONSTERS.Cyberdemon.room_size
        infight_damage = DOOM.MONSTERS.Cyberdemon.infight_damage
    }
    CyberdemonLord =
    {
        id = 8758
        r = 130
        h = 460
        level = 8
        boss_type = DOOM.MONSTERS.Cyberdemon.boss_type
        boss_prob = DOOM.MONSTERS.Cyberdemon.boss_prob
        prob = DOOM.MONSTERS.Cyberdemon.prob
        crazy_prob = DOOM.MONSTERS.Cyberdemon.crazy_prob
        health = 20000
        damage = DOOM.MONSTERS.Cyberdemon.damage
        attack = DOOM.MONSTERS.Cyberdemon.attack
        density = DOOM.MONSTERS.Cyberdemon.density
        weap_min_damage = DOOM.MONSTERS.Cyberdemon.weap_min_damage
        weap_prefs = DOOM.MONSTERS.Cyberdemon.weap_prefs
        room_size = DOOM.MONSTERS.Cyberdemon.room_size
        infight_damage = DOOM.MONSTERS.Cyberdemon.infight_damage
    }
}
