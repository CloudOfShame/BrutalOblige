----------------------------------------------------------------
--
--  Copyright (C) 2009 Enhas
--  Copyright (C) 2009 Andrew Apted
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version 2
--  of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
----------------------------------------------------------------

BRUTALSTEALTH = { }

BRUTALSTEALTH.MONSTERS =
{
  stealth_PistolZombie =
  {
    id = 352
    r = BRUTALDOOM.MONSTERS.PistolZombie.r
    h = BRUTALDOOM.MONSTERS.PistolZombie.h
    replaces = "PistolZombie"
    replace_prob = 30
    theme_prob=30
    crazy_prob = BRUTALDOOM.MONSTERS.PistolZombie.crazy_prob
    health = BRUTALDOOM.MONSTERS.PistolZombie.health
    damage = BRUTALDOOM.MONSTERS.PistolZombie.damage
    attack = BRUTALDOOM.MONSTERS.PistolZombie.attack
    give = BRUTALDOOM.MONSTERS.PistolZombie.give
    density = BRUTALDOOM.MONSTERS.PistolZombie.density
    room_size=BRUTALDOOM.MONSTERS.PistolZombie.room_size
    disloyal=BRUTALDOOM.MONSTERS.PistolZombie.disloyal
    trap_factor=BRUTALDOOM.MONSTERS.PistolZombie.trap_factor
    infight_damage=BRUTALDOOM.MONSTERS.PistolZombie.infight_damage
    float=BRUTALDOOM.MONSTERS.PistolZombie.float
    theme=BRUTALDOOM.MONSTERS.PistolZombie.theme
    invis = true
    allow_in_theme=BRUTALDOOM.MONSTERS.PistolZombie.allow_in_theme
  }
  
  stealth_Belphegor =
  {
    id = 353
    r = BRUTALDOOM.MONSTERS.Belphegor.r
    h = BRUTALDOOM.MONSTERS.Belphegor.h
    replaces = "Belphegor"
    replace_prob = 30
    theme_prob=30
    crazy_prob = BRUTALDOOM.MONSTERS.Belphegor.crazy_prob
    health = BRUTALDOOM.MONSTERS.Belphegor.health
    damage = BRUTALDOOM.MONSTERS.Belphegor.damage
    attack = BRUTALDOOM.MONSTERS.Belphegor.attack
    give = BRUTALDOOM.MONSTERS.Belphegor.give
    density = BRUTALDOOM.MONSTERS.Belphegor.density
    room_size=BRUTALDOOM.MONSTERS.Belphegor.room_size
    disloyal=BRUTALDOOM.MONSTERS.Belphegor.disloyal
    trap_factor=BRUTALDOOM.MONSTERS.Belphegor.trap_factor
    infight_damage=BRUTALDOOM.MONSTERS.Belphegor.infight_damage
    float=BRUTALDOOM.MONSTERS.Belphegor.float
    theme=BRUTALDOOM.MONSTERS.Belphegor.theme
    invis = true
    allow_in_theme=BRUTALDOOM.MONSTERS.Belphegor.allow_in_theme
  }
  
  stealth_Juggernaut =
  {
    id = 354
    r = BRUTALDOOM.MONSTERS.Juggernaut.r
    h = BRUTALDOOM.MONSTERS.Juggernaut.h
    replaces = "Juggernaut"
    replace_prob = 30
    theme_prob=30
    crazy_prob = BRUTALDOOM.MONSTERS.Juggernaut.crazy_prob
    health = BRUTALDOOM.MONSTERS.Juggernaut.health
    damage = BRUTALDOOM.MONSTERS.Juggernaut.damage
    attack = BRUTALDOOM.MONSTERS.Juggernaut.attack
    give = BRUTALDOOM.MONSTERS.Juggernaut.give
    density = BRUTALDOOM.MONSTERS.Juggernaut.density
    room_size=BRUTALDOOM.MONSTERS.Juggernaut.room_size
    disloyal=BRUTALDOOM.MONSTERS.Juggernaut.disloyal
    trap_factor=BRUTALDOOM.MONSTERS.Juggernaut.trap_factor
    infight_damage=BRUTALDOOM.MONSTERS.Juggernaut.infight_damage
    float=BRUTALDOOM.MONSTERS.Juggernaut.float
    theme=BRUTALDOOM.MONSTERS.Juggernaut.theme
    invis = true
    allow_in_theme=BRUTALDOOM.MONSTERS.Juggernaut.allow_in_theme
  }
  
  stealth_Zyberdemon =
  {
    id = 355
    r = BRUTALDOOM.MONSTERS.Zyberdemon.r
    h = BRUTALDOOM.MONSTERS.Zyberdemon.h
    replaces = "Zyberdemon"
    replace_prob = 30
    theme_prob=30
    crazy_prob = BRUTALDOOM.MONSTERS.Zyberdemon.crazy_prob
    health = BRUTALDOOM.MONSTERS.Zyberdemon.health
    damage = BRUTALDOOM.MONSTERS.Zyberdemon.damage
    attack = BRUTALDOOM.MONSTERS.Zyberdemon.attack
    give = BRUTALDOOM.MONSTERS.Zyberdemon.give
    density = BRUTALDOOM.MONSTERS.Zyberdemon.density
    room_size=BRUTALDOOM.MONSTERS.Zyberdemon.room_size
    disloyal=BRUTALDOOM.MONSTERS.Zyberdemon.disloyal
    trap_factor=BRUTALDOOM.MONSTERS.Zyberdemon.trap_factor
    infight_damage=BRUTALDOOM.MONSTERS.Zyberdemon.infight_damage
    float=BRUTALDOOM.MONSTERS.Zyberdemon.float
    theme=BRUTALDOOM.MONSTERS.Zyberdemon.theme
    invis = true
    allow_in_theme=BRUTALDOOM.MONSTERS.Zyberdemon.allow_in_theme
  }
  
  stealth_HeadlessZombie =
  {
    id = 356
    r = BRUTALDOOM.MONSTERS.HeadlessZombie.r
    h = BRUTALDOOM.MONSTERS.HeadlessZombie.h
    replaces = "HeadlessZombie"
    replace_prob = 30
    theme_prob=30
    crazy_prob = BRUTALDOOM.MONSTERS.HeadlessZombie.crazy_prob
    health = BRUTALDOOM.MONSTERS.HeadlessZombie.health
    damage = BRUTALDOOM.MONSTERS.HeadlessZombie.damage
    attack = BRUTALDOOM.MONSTERS.HeadlessZombie.attack
    give = BRUTALDOOM.MONSTERS.HeadlessZombie.give
    density = BRUTALDOOM.MONSTERS.HeadlessZombie.density
    room_size=BRUTALDOOM.MONSTERS.HeadlessZombie.room_size
    disloyal=BRUTALDOOM.MONSTERS.HeadlessZombie.disloyal
    trap_factor=BRUTALDOOM.MONSTERS.HeadlessZombie.trap_factor
    infight_damage=BRUTALDOOM.MONSTERS.HeadlessZombie.infight_damage
    float=BRUTALDOOM.MONSTERS.HeadlessZombie.float
    theme=BRUTALDOOM.MONSTERS.HeadlessZombie.theme
    invis = true
    allow_in_theme=BRUTALDOOM.MONSTERS.HeadlessZombie.allow_in_theme
  }
  
  stealth_Labguy =
  {
    id = 357
    r = BRUTALDOOM.MONSTERS.Labguy.r
    h = BRUTALDOOM.MONSTERS.Labguy.h
    replaces = "Labguy"
    replace_prob = 30
    theme_prob=30
    crazy_prob = BRUTALDOOM.MONSTERS.Labguy.crazy_prob
    health = BRUTALDOOM.MONSTERS.Labguy.health
    damage = BRUTALDOOM.MONSTERS.Labguy.damage
    attack = BRUTALDOOM.MONSTERS.Labguy.attack
    give = BRUTALDOOM.MONSTERS.Labguy.give
    density = BRUTALDOOM.MONSTERS.Labguy.density
    room_size=BRUTALDOOM.MONSTERS.Labguy.room_size
    disloyal=BRUTALDOOM.MONSTERS.Labguy.disloyal
    trap_factor=BRUTALDOOM.MONSTERS.Labguy.trap_factor
    infight_damage=BRUTALDOOM.MONSTERS.Labguy.infight_damage
    float=BRUTALDOOM.MONSTERS.Labguy.float
    theme=BRUTALDOOM.MONSTERS.Labguy.theme
    invis = true
    allow_in_theme=BRUTALDOOM.MONSTERS.Labguy.allow_in_theme
  }
  
  stealth_AncientArachnotron =
  {
    id = 358
    r = BRUTALDOOM.MONSTERS.AncientArachnotron.r
    h = BRUTALDOOM.MONSTERS.AncientArachnotron.h
    replaces = "AncientArachnotron"
    replace_prob = 30
    theme_prob=30
    crazy_prob = BRUTALDOOM.MONSTERS.AncientArachnotron.crazy_prob
    health = BRUTALDOOM.MONSTERS.AncientArachnotron.health
    damage = BRUTALDOOM.MONSTERS.AncientArachnotron.damage
    attack = BRUTALDOOM.MONSTERS.AncientArachnotron.attack
    give = BRUTALDOOM.MONSTERS.AncientArachnotron.give
    density = BRUTALDOOM.MONSTERS.AncientArachnotron.density
    room_size=BRUTALDOOM.MONSTERS.AncientArachnotron.room_size
    disloyal=BRUTALDOOM.MONSTERS.AncientArachnotron.disloyal
    trap_factor=BRUTALDOOM.MONSTERS.AncientArachnotron.trap_factor
    infight_damage=BRUTALDOOM.MONSTERS.AncientArachnotron.infight_damage
    float=BRUTALDOOM.MONSTERS.AncientArachnotron.float
    theme=BRUTALDOOM.MONSTERS.AncientArachnotron.theme
    invis = true
    allow_in_theme=BRUTALDOOM.MONSTERS.AncientArachnotron.allow_in_theme
  }
  
  stealth_Volcabus =
  {
    id = 359
    r = BRUTALDOOM.MONSTERS.Volcabus.r
    h = BRUTALDOOM.MONSTERS.Volcabus.h
    replaces = "Volcabus"
    replace_prob = 30
    theme_prob=30
    crazy_prob = BRUTALDOOM.MONSTERS.Volcabus.crazy_prob
    health = BRUTALDOOM.MONSTERS.Volcabus.health
    damage = BRUTALDOOM.MONSTERS.Volcabus.damage
    attack = BRUTALDOOM.MONSTERS.Volcabus.attack
    give = BRUTALDOOM.MONSTERS.Volcabus.give
    density = BRUTALDOOM.MONSTERS.Volcabus.density
    room_size=BRUTALDOOM.MONSTERS.Volcabus.room_size
    disloyal=BRUTALDOOM.MONSTERS.Volcabus.disloyal
    trap_factor=BRUTALDOOM.MONSTERS.Volcabus.trap_factor
    infight_damage=BRUTALDOOM.MONSTERS.Volcabus.infight_damage
    float=BRUTALDOOM.MONSTERS.Volcabus.float
    theme=BRUTALDOOM.MONSTERS.Volcabus.theme
    invis = true
    allow_in_theme=BRUTALDOOM.MONSTERS.Volcabus.allow_in_theme
  }
  
  stealth_Mummy =
  {
    id = 360
    r = BRUTALDOOM.MONSTERS.Mummy.r
    h = BRUTALDOOM.MONSTERS.Mummy.h
    replaces = "Mummy"
    replace_prob = 30
    theme_prob=30
    crazy_prob = BRUTALDOOM.MONSTERS.Mummy.crazy_prob
    health = BRUTALDOOM.MONSTERS.Mummy.health
    damage = BRUTALDOOM.MONSTERS.Mummy.damage
    attack = BRUTALDOOM.MONSTERS.Mummy.attack
    give = BRUTALDOOM.MONSTERS.Mummy.give
    density = BRUTALDOOM.MONSTERS.Mummy.density
    room_size=BRUTALDOOM.MONSTERS.Mummy.room_size
    disloyal=BRUTALDOOM.MONSTERS.Mummy.disloyal
    trap_factor=BRUTALDOOM.MONSTERS.Mummy.trap_factor
    infight_damage=BRUTALDOOM.MONSTERS.Mummy.infight_damage
    float=BRUTALDOOM.MONSTERS.Mummy.float
    theme=BRUTALDOOM.MONSTERS.Mummy.theme
    invis = true
    allow_in_theme=BRUTALDOOM.MONSTERS.Mummy.allow_in_theme
  }
  
  stealth_D4caco =
  {
    id = 361
    r = BRUTALDOOM4.MONSTERS.D4caco.r
    h = BRUTALDOOM4.MONSTERS.D4caco.h
    replaces = "D4caco"
    replace_prob = 30
    theme_prob=30
    crazy_prob = BRUTALDOOM4.MONSTERS.D4caco.crazy_prob
    health = BRUTALDOOM4.MONSTERS.D4caco.health
    damage = BRUTALDOOM4.MONSTERS.D4caco.damage
    attack = BRUTALDOOM4.MONSTERS.D4caco.attack
    give = BRUTALDOOM4.MONSTERS.D4caco.give
    density = BRUTALDOOM4.MONSTERS.D4caco.density
    room_size=BRUTALDOOM4.MONSTERS.D4caco.room_size
    disloyal=BRUTALDOOM4.MONSTERS.D4caco.disloyal
    trap_factor=BRUTALDOOM4.MONSTERS.D4caco.trap_factor
    infight_damage=BRUTALDOOM4.MONSTERS.D4caco.infight_damage
    float=BRUTALDOOM4.MONSTERS.D4caco.float
    theme=BRUTALDOOM4.MONSTERS.D4caco.theme
    invis = true
    allow_in_theme=BRUTALDOOM4.MONSTERS.D4caco.allow_in_theme
  }
  
  --original stealth monsters copied from the module included with Oblige 7.70
  --some edits eg where the original monster has been replaced
  stealth_RifleZombie =
  {
    id = 9061
    r = 20
    h = 56 
    replaces = "RifleZombie"
    replace_prob = 30
    crazy_prob = 5
    health = 20
    damage = 4
    attack = "hitscan"
    give = BRUTALDOOM.MONSTERS.RifleZombie.give
    density = 1.5
    invis = true
  }

  stealth_shooter =
  {
    id = 9060
    r = 20
    h = 56 
    replaces = "shooter"
    replace_prob = 20
    crazy_prob = 11
    health = 30
    damage = 10
    attack = "hitscan"
    give = { {weapon="shotty"}, {ammo="shell",count=4} }
    invis = true
  }

  stealth_imp =
  {
    id = 9057
    r = 20
    h = 56 
    replaces = "imp"
    replace_prob = 40
    crazy_prob = 25
    health = 60
    damage = 20
    attack = "missile"
    invis = true
  }

  stealth_demon =
  {
    id = 9055
    r = 30
    h = 56 
    replaces = "demon"
    replace_prob = 40
    crazy_prob = 30
    health = 150
    damage = 25
    attack = "melee"
    invis = true
  }

  stealth_caco =
  {
    id = 9053
    r = 31
    h = 56 
    replaces = "caco"
    replace_prob = 25
    theme_prob=25
    crazy_prob = 41
    health = 400
    damage = 35
    attack = "missile"
    density = 0.5
    float = true
    invis = true
    allow_in_theme=BRUTALDOOM.MONSTERS.caco.allow_in_theme
  }

  stealth_baron =
  {
    id = 9052
    r = 24
    h = 64 
    replaces = "baron"
    replace_prob = 20
    crazy_prob = 10
    health = 1000
    damage = 45
    attack = "missile"
    density = 0.5
    invis = true
  }
  
  stealth_gunner =
  {
    id = 9054
    r = 20
    h = 56 
    replaces = "gunner"
    replace_prob = 20
    crazy_prob = 21
    health = 70
    damage = 50
    attack = "hitscan"
    give = BRUTALDOOM.MONSTERS.gunner.give
    invis = true
  }

  stealth_revenant =
  {
    id = 9059
    r = 20
    h = 64 
    replaces = "revenant"
    replace_prob = 30
    crazy_prob = 40
    health = 300
    damage = 70
    attack = "missile"
    density = 0.6
    invis = true
  }

  stealth_knight =
  {
    id = 9056
    r = 24
    h = 64 
    replaces = "knight"
    replace_prob = 25
    crazy_prob = 11
    health = 500
    damage = 45
    attack = "missile"
    density = 0.7
    invis = true
  }

  stealth_mancubus =
  {
    id = 9058
    r = 48
    h = 64 
    replaces = "mancubus"
    replace_prob = 25
    crazy_prob = 31
    health = 600
    damage = 70
    attack = "missile"
    density = 0.6
    invis = true
  }

  stealth_arach =
  {
    id = 9050
    r = 66
    h = 64 
    replaces = "arach"
    replace_prob = 25
    crazy_prob = 11
    health = 500
    damage = 70
    attack = "missile"
    density = 0.8
    invis = true
  }

  stealth_vile =
  {
    id = 9051
    r = 20
    h = 56 
    replaces = "vile"
    replace_prob = 10
    crazy_prob = 5
    health = 700
    damage = 40
    attack = "hitscan"
    density = 0.2
    nasty = true
    invis = true
  }
  
}


BRUTALSTEALTH.CHOICES =
{
  "normal", _("Normal"),
  "less",   _("Less"),
  "more",   _("More"),
}


function BRUTALSTEALTH.setup(self)
  -- apply the Quantity choice
  local qty = self.options.qty.value

  for name,_ in pairs(BRUTALSTEALTH.MONSTERS) do
    local M = GAME.MONSTERS[name]
    
    if M and qty == "less" then
      M.replace_prob = M.replace_prob / 2
      if M.crazy_prob then
        M.crazy_prob = M.crazy_prob / 3
      end
    end

    if M and qty == "more" then
      M.replace_prob = math.max(80, M.replace_prob * 2)
      if M.crazy_prob then
        M.crazy_prob = M.crazy_prob * 3
      end
    end

    -- EDGE uses different id numbers -- fix them
    if M and OB_CONFIG.game == "edge" then
      M.id = EDGE_IDS[name]
    end
  end
end


OB_MODULES["brutal_stealth_mons"] =
{
  label = _("Brutal Stealth Monsters")

  game = "brutaldoom"

  engine = { edge=1, zdoom=1, gzdoom=1, skulltag=1 }

  tables =
  {
    BRUTALSTEALTH
  }

  hooks =
  {
    setup = BRUTALSTEALTH.setup
  }

  options =
  {
    qty =
    {
      label = _("Default Quantity")
      choices = BRUTALSTEALTH.CHOICES
    }
  }
}


----------------------------------------------------------------


BRUTALSTEALTH.CONTROL_CHOICES =
{
  "default", _("DEFAULT"),
  "none",    _("None at all"),
  "scarce",  _("Scarce"),
  "less",    _("Less"),
  "plenty",  _("Plenty"),
  "more",    _("More"),
  "heaps",   _("Heaps"),
  "insane",  _("INSANE"),
}

BRUTALSTEALTH.CONTROL_PROBS =
{
  none   = 0
  scarce = 2
  less   = 15
  plenty = 50
  more   = 120
  heaps  = 300
  insane = 2000
}


function BRUTALSTEALTH.control_setup(self)
  for name,opt in pairs(self.options) do
    local M = GAME.MONSTERS[name]

    if M and opt.value != "default" then
      local prob = BRUTALSTEALTH.CONTROL_PROBS[opt.value]

      M.replaces = nil
      M.prob = prob
      M.crazy_prob = prob

      if prob > 80 then M.density = 1.0 end
    end
  end -- for opt
end


OB_MODULES["brutal_stealth_mon_control"] =
{
  label = _("Brutal Stealth Monsters : Control")

  module = "brutal_stealth_mons"

  hooks =
  {
    setup = BRUTALSTEALTH.control_setup
  }

  options =
  {
    stealth_Belphegor   = { label="Belphegor",     choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_Juggernaut   = { label="Juggernaut",     choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_Zyberdemon   = { label="Zyberdemon",     choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_HeadlessZombie   = { label="Headless Zombie",     choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_Labguy   = { label="Labguy",     choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_AncientArachnotron   = { label="Ancient Arachnotron",     choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_Volcabus   = { label="Volcabus",     choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_Mummy   = { label="Mummy",     choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_PistolZombie   = { label="Pistol Zombieman",     choices=BRUTALSTEALTH.CONTROL_CHOICES }
    
    --original monsters
    stealth_RifleZombie   = { label="Rifle Zombieman",     choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_shooter  = { label=_("Shotgun Guy"),   choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_imp      = { label=_("Imp"),           choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_demon    = { label=_("Demon"),         choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_caco     = { label=_("Cacodemon"),     choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_baron    = { label=_("Baron of Hell"), choices=BRUTALSTEALTH.CONTROL_CHOICES }

    stealth_gunner   = { label=_("Chaingunner"),   choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_knight   = { label=_("Hell Knight"),   choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_revenant = { label=_("Revenant"),      choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_mancubus = { label=_("Mancubus"),      choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_arach    = { label=_("Arachnotron"),   choices=BRUTALSTEALTH.CONTROL_CHOICES }
    stealth_vile     = { label=_("Arch-vile"),     choices=BRUTALSTEALTH.CONTROL_CHOICES }
  }
}


