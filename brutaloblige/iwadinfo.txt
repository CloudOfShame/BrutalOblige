IWad
{
	Name = "Blasphemer"
	Autoname = "blasphemer"
	Game = "Doom"
	Config = "Doom"
	Mapinfo = "mapinfo/doom2.txt"
	MustContain = "E1M1", "E2M1", "TITLE", "BLASPHEM"
	BannerColors = "73 00 00", "00 00 00"
}

IWad
{
	Name = "Heretic: Shadow of the Serpent Riders"
	Autoname = "heretic.shadow"
	Game = "Doom"
	Config = "Doom"
	Mapinfo = "mapinfo/doom2.txt"
	Compatibility = "Extended"
	MustContain = "E1M1", "E2M1", "TITLE", "MUS_E1M1", "EXTENDED"
	BannerColors = "fc fc 00", "a8 00 00"
}

IWad
{
	Name = "Heretic"
	Game = "Doom"
	Config = "Doom"
	Autoname = "heretic.heretic"
	Mapinfo = "mapinfo/doom2.txt"
	MustContain = "E1M1", "E2M1", "TITLE", "MUS_E1M1"
	BannerColors = "fc fc 00", "a8 00 00"
}

IWad
{
	Name = "Heretic Shareware"
	Game = "Doom"
	Config = "Doom"
	Mapinfo = "mapinfo/doom2.txt"
	Compatibility = "Shareware"
	MustContain = "E1M1", "TITLE", "MUS_E1M1"
	BannerColors = "fc fc 00", "a8 00 00"
}